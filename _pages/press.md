---
layout: default
permalink: /press
lang: fr
ref: press
---

# Presse papier

[Health Data Hub et Covid-19 : la sécurité des données médicales en débat devant le Conseil d'État](https://www.lequotidiendumedecin.fr/actu-pro/politique-de-sante/health-data-hub-et-covid-19-la-securite-des-donnees-medicales-en-debat-devant-le-conseil-detat) <em><span style="color:orange;">12 juin</span></em>

[Les données de santé de la population française, collectées massivement, seront-elles vraiment protégées ?](https://www.bastamag.net/Donnees-de-sante-Microsoft-Gafam-Health-Data-Hub-Covid-fichier-Big-Data) <em><span style="color:orange;">11 juin</span></em>
- "La mutuelle Malakoff Médéric a aussi pu avoir accès à des données pour « mesurer et comprendre les restes à charge réels des patients »"
- "Ainsi OPBI Santé, fondée par deux « consultants en organisation hospitalière », souhaite par exemple accéder aux données pour déterminer les durées de séjour qui seraient les plus efficientes pour chaque type de pathologie. « Des durées atteignables et pertinentes, mais nécessitant les meilleures pratiques organisationnelles », vante la start-up."

[Health Data Hub : des organisations dénoncent un passage en force du projet dans le contexte d'urgence sanitaire](https://www.zdnet.fr/actualites/health-data-hub-des-organisations-denoncent-un-passage-en-force-du-projet-dans-le-contexte-d-urgence-sanitaire-39905045.htm) <em><span style="color:orange;">11 juin</span></em>
- "Les requérants demandent au Conseil d'Etat de suspendre l'arrêté en date du 21 avril 2020 confiant les données de santé liées au Covid-19 au Health Data Hub"
- "Les requêtes déposées au Conseil d'Etat portent sur deux aspects de la protection des données : d'un côté, le risque de voir un Etat tiers (les Etats-Unis en l'occurrence) accéder aux données de santé françaises et, de l'autre, l'absence d'information préalable à destination du public (prévue par le RGPD)."
- "La plateforme sera alimentée au cas par cas, au fur et à mesure des projets, rappelle Stéphanie Combes. On y trouve pour l'heure les données du réseau "Oscour" (Organisation de la surveillance coordonnée des urgences) issues de Santé Publique France, permettant de suivre les passages aux urgences pendant l'épidémie."

[Health Data Hub : « Le choix de Microsoft est un contresens industriel ! »](https://amp.lepoint.fr/2379394?utm_term=Autofeed&utm_medium=Social&utm_source=Twitter&Echobox=1591814194&__twitter_impression=true) <em><span style="color:orange;">10 juin</span></em>
- "Il s'agit d'un signal politique inquiétant et aussi d'un contresens industriel ! Le choix de la société Microsoft pour assurer l'hébergement du Health Data Hub a été effectué sans créer un appel d'offres spécifique et a été mis en avant pour des raisons de conformité avec les prérequis de ce projet. Ces prérequis doivent aujourd'hui être remis en question."
- "Mais Microsoft aura à cœur de se rendre indispensable ! En effet, si la réversibilité du choix de Microsoft a souvent été soulignée, il faut se rappeler que le « cœur de métier » des sociétés qui assurent l'hébergement et le traitement en masse des données est justement d'ajouter progressivement des fonctions « propriétaires » afin d'empêcher que leurs clients ne puissent migrer facilement vers d'autres plateformes."
- "En raison de la diversité et du volume des données qu'il devrait rassembler, le Health Data Hub constitue l'un des plus importants projets d'administration électronique jamais menés en France et certainement l'un des plus stratégiques pour les géants des technologies."

[Le Health Data Hub attaqué devant le Conseil d’Etat](https://www.mediapart.fr/journal/france/090620/le-health-data-hub-attaque-devant-le-conseil-d-etat?onglet=full) <em><span style="color:orange;">9 juin</span></em>
- "Une  quinzaine  de  personnalités  et  d’organisationsont  déposé  un  référé-liberté  contre  le  déploiement,accéléré  au  nom  de  l’état  d’urgence  sanitaire,  dela  nouvelle  plateforme  de  santé  devant  centraliserl’intégralité   de   nos   données   de   santé   et   dontl’hébergement a été confié à Microsoft."
- "Ils ont cette fois été rejoints par le collectif InterHop,composé  de  professionnels  du  secteur  de  la  santé  etde l’informatique médicale, mobilisé depuis près d’unan  contre  le  projet  mais  également  par  le  médecinDidier  Sicard,  ancien  président  du  Comité  nationalconsultatif  d’éthique,  le  professeur  Bernard  Fallery,spécialiste  des  systèmes  d’information,  le  Syndicatnational  des  journalistes  (SNJ),  le  Syndicat  de  lamédecine générale (SMG), l’Union française pour unemédecine libre (UFML), la représentante des usagersdu conseil de surveillance de l’APHP, l’Observatoirede la transparence dans les politiques de médicaments,l’Union générale des ingénieurs, cadres et techniciensCGT  (UGICT-CGT)  et  l’Union  fédérale  médecins,ingénieurs,  cadres,  techniciens  CGT  Santé  et  Actionsociale (UFMICT-CGT Santé et Action sociale)."
- "Health Data Hub « porte une atteinte grave et sûrementirréversible  aux  droits  de  67  millions  d’habitantsde   disposer   de   la   protection   de   leur   vie   privéenotamment  celle  de  leurs  données  parmi  les  plusintimes,  protégées  de  façon  absolue  par  le  secretmédical : leurs données de santé »"

[La France et l'Allemagne défendent un cloud souverain européen](https://www.lesechos.fr/tech-medias/hightech/le-cloud-europeen-franco-allemand-devoile-ses-services-numeriques-souverains-1208046) <em><span style="color:orange;">5 juin</span></em>
- "Avec Gaia-X, la France et l'Allemagne parrainent un catalogue d'offres de stockage et de traitement de données porté par des acteurs transparents en matière de sécurité des données."
- "catalogue de services numériques portés par des hébergeurs et des éditeurs de logiciels qui se seront préalablement engagés sur des standards de nature à renforcer la confiance de leurs clients en matière de sécurité des données mais aussi de transparence des contrats"
- "Il s'agit aussi de protéger les secrets industriels des entreprises contre les lois extraterritoriales qui autorisent, dans certains cas, la perquisition de données par des justices étrangères, américaines ou chinoises."

[La France mise 500 millions pour protéger ses start-up de l’appétit des Gafa](https://www.lefigaro.fr/secteur/high-tech/la-france-mise-500-millions-pour-proteger-ses-start-up-de-l-appetit-des-gafa-20200604) <em><span style="color:orange;">5 juin</span></em>
- "Bruno Le Maire et Cédric O vont présenter un plan destiné à préserver la souveraineté nationale dans le numérique, dont le montant total s’élève à 1,2 milliard d’euros."
- "Or, dans certains domaines, la France, pays aux 13.000 start-up, disposent de pépites qui sont autant de «proies», pouvant être rachetées par des géants du numériques extra-territoriaux, essentiellement américains"

[L'Etat choisit Microsoft pour les données de santé et crée la polémique](https://www.lesechos.fr/tech-medias/hightech/letat-choisit-microsoft-pour-les-donnees-de-sante-et-cree-la-polemique-1208376) <em><span style="color:orange;">5 juin</span></em>
- "Le gouvernement français a pris la décision d'héberger les informations de santé de millions de Français sur les serveurs de l'américain Microsoft, au détriment d'OVH, une société française."
- "La polémique ne pouvait pas éclater à un pire moment. Quelques jours avant de lancer avec l'Allemagne le projet d'informatique en ligne européenne Gaia-X pour la protection des données dans le « cloud computing », le gouvernement français doit défendre sa décision d'héberger les informations de santé de millions de Français sur les serveurs de l'américain Microsoft…"
- "« Il faut être rapide parce que le Health Data Hub sauve des vies »"

[Opinion Le Health Data Hub, un outil à la pointe de l'innovation numérique publique](https://www.lesechos.fr/idees-debats/cercle/opinion-le-health-data-hub-un-outil-a-la-pointe-de-linnovation-numerique-publique-1208487) <em><span style="color:orange;">5 juin</span></em>

[« La politique publique des données de santé est à réinventer »](https://www.lemonde.fr/idees/article/2020/06/04/la-politique-publique-des-donnees-de-sante-est-a-reinventer_6041706_3232.html) <em><span style="color:orange;">4 juin</span></em>
- "Il faut donc remettre à plat le sujet, avec des orientations claires : rétablir la confiance et définir une
stratégie ; couvrir l’ensemble du champ sanitaire et médico-social ; simplifier l’accès pour permettre
d’aller vite ; développer l’utilisation des données en temps réel pour repérer les problèmes
émergents ; ancrer l’architecture technique dans un écosystème décentralisé, respectueux des acteurs
et propice à une maîtrise « souveraine » de l’hébergement des données ; garantir le respect du secret
médical et du droit à la vie privée ; favoriser l’émergence de nouvelles technologies dans le respect
d’une éthique et d’une déontologie exigeantes"

[Gaia-X : la France et l'Allemagne lancent leur Airbus européen du cloud](https://www.journaldunet.com/web-tech/cloud/1491859-gaia-x-la-france-et-l-allemagne-lancent-leur-airbus-europeen-du-cloud/) <em><span style="color:orange;">4 juin</span></em>

[Pour une souveraineté européenne de la santé](https://forumatena.org/pour-une-souverainete-europeenne-de-la-sante/) <em><span style="color:orange;">27 mai</span></em>
- "Peut-on compter sur la pseudonymisation quand les attributs de chaque individu se comptent par milliers ?"
- "Le comité (CESREES) en charge de se prononcer sur le caractère d’intérêt public des projets n’exercera son filtre que sporadiquement : seule une requête du ministre de la Santé, de la CNIL ou du CESREES lui-même provoquera l’examen. La CNIL avait pourtant recommandé une étude systématique."

[Données de santé : l’arbre StopCovid qui cache la forêt Health Data Hub](https://theconversation.com/donnees-de-sante-larbre-stopcovid-qui-cache-la-foret-health-data-hub-138852) <em><span style="color:orange;">25 mai</span></em>
- "C’est une question de politique nationale, déjà soulevée dans The Conversation France, puisqu’il s’agit de faire gérer un bien public par un acteur privé, et sans espoir de réversibilité. Mais aussi une question politique de souveraineté numérique européenne puisque cet acteur étasunien se trouve soumis au Cloud Act, loi de 2018 qui permet aux juges américains de demander l’accès aux données sur des serveurs situés en dehors des États-Unis."

[Health Data Hub : pourquoi cette plateforme inquiète tant ?](https://www.presse-citron.net/health-data-hub-pourquoi-cette-plateforme-inquiete-tant/) <em><span style="color:orange;">18 mai</span></em>
- "Le rassemblement de ces données sera complété par deux fichiers récents liés au Covid-19, le SIDEP qui regroupe les données de Laboratoire et le Contact Covid où sont inscrites les personnes potentiellement contaminées au Covid-19. Autrement dit un nombre considérable de données de santé seront stockées via ce nouveau système."
- "L’objectif affiché de Health Data Hub serait de rendre le système de santé plus efficace et d’aider à la gestion de la crise sanitaire que nous traversons actuellement. À terme, toutes les données enregistrées sur le site de la sécurité sociale seront automatiquement intégrées au fichier."

[Health Data Hub: pourquoi le fichier national qui abritera nos données de santé suscite des craintes](https://www.bfmtv.com/tech/health-data-hub-pourquoi-le-fichier-national-qui-abritera-nos-donnees-de-sante-suscite-des-craintes-1913849.html) <em><span style="color:orange;">17 mai</span></em>

[Health Data Hub : nos données de santé vont-elles être livrées aux Américains ?](https://www.01net.com/actualites/health-data-hub-nos-donnees-de-sante-vont-elles-etre-livrees-aux-americains-1913351.html) <em><span style="color:orange;">14 mai</span></em>
- "le gouvernement passe en force son projet de plate-forme sanitaire géante au nom de l'état d'urgence. Problème : les données seraient hébergées par Microsoft qui pourrait sur seul ordre des États-Unis... les transférer outre-Atlantique"

[Thomas Gomart (IFRI) : « Le Covid-19 accélère le changement de mains de pans entiers de l'activité économique au profit des plateformes numériques »](https://www.lesechos.fr/amp/1201806) <em><span style="color:orange;">13 mai</span></em>
- "les données des consommateurs européens ont été exploitées par les plateformes américaines bien au-delà de ce que les utilisateurs pouvaient savoir. L’affaire Snowden, en 2013, a révélé au grand jour certains mécanismes."
- "Les plateformes américaines sont parties prenantes du complexe militaro-numérique. Tout en étant des entreprises privées versant peu de dividendes mais investissant beaucoup, elles sont des vecteurs de la puissance américaine. En peu de temps, elles ont inventé et offert des services auxquels les Européens ne veulent pas renoncer. Le danger, c’est de se focaliser sur le service sans vouloir voir l’architecture de puissance."
- "L’enjeu fondamental pour les Européens est d’être en mesure de conserver leur autonomie de pensée."

[Données de santé : pourquoi (et comment) vont-elles être hébergées sur des serveurs de Microsoft ?](https://www-lci-fr.cdn.ampproject.org/c/s/www.lci.fr/amp/sante/donnees-de-sante-pourquoi-et-comment-vont-elles-etre-hebergees-sur-des-serveurs-de-microsoft-2153527.html) <em><span style="color:orange;">11 mai</span></em>
- "un flicage éhonté de vos informations de santé"

[La Cnil s’inquiète d’un possible transfert de nos données de santé aux Etats-Unis](https://www.mediapart.fr/journal/france/080520/la-cnil-s-inquiete-d-un-possible-transfert-de-nos-donnees-de-sante-aux-etats-unis) <em><span style="color:orange;">8 mai</span></em>
- "La Cnil aurait-elle alors mal lu le contrat ? « Je ne dis pas ça. Mais je trouve que les faits sont un peu détournés. En tout cas, nous avons bien indiqué que les données ne pourront pas être transférées. Je peux même vous dire que c’est à la page 11 du contrat.»"

[Le Health Data Hub ou le risque d’une santé marchandisée](https://lvsl.fr/le-health-data-hub-ou-le-risque-dune-sante-marchandisee/)
- "Il semble qu’ici se loge une fois de plus la contradiction de fond entre la logique du soin inconditionnel propre au secteur public ainsi qu’au serment d’Hippocrate et les exigences d’efficacité et de rentabilité aujourd’hui dénoncées par les personnels médicaux et hospitaliers à travers leurs mouvements de grève et leurs réactions à la crise du Covid-19"

[Données de santé: l’Etat accusé de favoritisme au profit de Microsoft](https://www.mediapart.fr/journal/france/110320/donnees-de-sante-l-etat-accuse-de-favoritisme-au-profit-de-microsoft)
- "Le non-respect des principes d’égalité et de transparence dans le choix de Microsoft Azure"

[Pour des données de santé au service des patients](https://interhop.org/donnees-de-sante-au-service-des-patients/)
- "Selon un récent sondage, l’hôpital est même l’institution en laquelle les Français ont le plus confiance. Quel serait l’impact d’une perte de confiance si des fuites de données massives étaient avérées ?"

[«Health Data Hub»: le méga fichier qui veut rentabiliser nos données de santé](https://www.mediapart.fr/journal/france/221119/health-data-hub-le-mega-fichier-qui-veut-rentabiliser-nos-donnees-de-sante)
- "Le gouvernement est-il en train de mettre en place un « big brother médical » et d’offrir nos données de santé aux géants du numérique ?"

[Opinion | Soignons nos données de santé](https://www.lesechos.fr/idees-debats/cercle/opinion-soignons-nos-donnees-de-sante-1143640)
- "Une fois brisée, la confiance ne se reforme plus"

[Données de santé & intelligence artificielle : rencontre avec Emmanuel Bacry](https://www.actuia.com/actualite/donnees-de-sante-intelligence-artificielle-rencontre-avec-emmanuel-bacry/)
- "J’aurais tendance à penser qu’il faut ouvrir totalement les données dans un endroit totalement sécurisé, et ensuite on contrôle ce qui se passe."

[Le Cloud Act, la riposte américaine au RGPD européen](https://www.lesechos.fr/idees-debats/cercle/le-cloud-act-la-riposte-americaine-au-rgpd-europeen-139429)
- "Mais toutes ces bonnes intentions n'ont pas pesé lourd face à la logique d'extraterritorialité (judiciaire et numérique) que prône l'administration américaine, laquelle a ratifié, toute juste deux mois avant la mise en oeuvre du RGPD, son Cloud Act, en élargissant ainsi ses prérogatives au monde entier. Le RGPD est de facto piétiné par le Cloud Act : particulièrement dans son article 48, qui précise notamment que les demandes de données par un pays tiers doivent être effectuées dans le cadre d'un accord international… alors que la nouvelle loi américaine est parfaitement unilatérale."

[Comment les GAFAM et Capgemini s'invitent dans la mine d'or des données médicales](https://www.lalettrea.fr/action-publique_executif/2019/02/18/comment-les-gafam-et-capgemini-s-invitent-dans-la-mine-d-or-des-donnees-medicales,108345136-ge0)

# Appel d'Offre à venir ?

[Protection des données de santé : MORIN-DESAILLY Catherine](http://videos.senat.fr/video.1710660_5f10400c7efbf.seance-publique-du-16-juillet-2020-apres-midi?timecode=16471000) <em><span style="color:orange;">16 juillet</span></em>

- Réponse de Monsieur Olivier Véran : "Les donnees de sante doivent rester de securisation et de gestion francaise." "Oui il aura un appel d'offre pour le fonctionnement au quotidien."

[https://www.economie.gouv.fr/direct-video-conference-presse-sur-application-stopcovid-23-juin#](https://www.economie.gouv.fr/direct-video-conference-presse-sur-application-stopcovid-23-juin#) <em><span style="color:orange;"> 23 juin</span></em>

- Monsieur Cédric O : "Il sera normal que dans les mois qui vienne, nous puissions lancer un appel d'offre pour avoir un choix plus large et des spécifications qui permettront à chacun de se positionner"

# Commission nationale de l'informatique et des libertés CNIL

[Mesures d’organisation et de fonctionnement du système de santé nécessaires pour faire face à l’épidémie de covid-19 dans le cadre de l’état d’urgence sanitaire ](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf) <em><span style="color:orange;">20 avril</span></em>

- "Le contrat mentionne l’existence de transferts de données en dehors de l’Union européenne dans le cadre du fonctionnement courant de la plateforme"
- "Les inquiétudes […] concernant l’accès par les autorités des États-Unis aux données transférées aux États-Unis, plus particulièrement la collecte et l’accès aux données personnelles à des fins de sécurité nationale"
- "Avec des algorithmes à l’état de l’art à partir de clés générées par les responsables de la plateforme sur un boîtier chiffrant maîtrisé par la plateforme des données de santé [...]. Elles seront conservées par l’hébergeur au sein d’un boîtier chiffrant, ce qui a pour conséquence de permettre techniquement à ce dernier d’accéder aux données"
- "Eu égard à la sensibilité des données en cause, que son hébergement et les services liés à sa gestion puissent être réservés à des entités relevant exclusivement des juridictions de l’Union européenne"

[Anonymisation vs. Pseudonymisation](https://www.cnil.fr/fr/identifier-les-donnees-personnelles)

[Avis sur un projet de loi relatif à l’organisation et à la transformation du système de santé](https://www.legifrance.gouv.fr/affichCnil.do?oldAction=rechExpCnil&id=CNILTEXT000038142154&fastReqId=177029739&fastPos=1)
- "Au-delà d’un simple élargissement, cette évolution change la dimension même du SNDS, qui viserait à contenir ainsi l’ensemble des données médicales donnant lieu à remboursement"

# Agence nationale de la sécurité des systèmes d'information ANSSI
[Sénat, audition de M. Guillaume Poupard, directeur général de l'ANSSI](https://videos.senat.fr/video.1608918_5ebaa04d6b8cd.audition-de-m-guillaume-poupard-directeur-general-de-l-agence-nationale-de-la-securite-des-systeme?timecode=4319000)  <em><span style="color:orange;">12 mai</span></em>
- "Pour insister sur les notions de souveraineté : ça m'inquiète de voir les géants du numérique avancer sur ces sujets de santé. On sait très bien qu'ils connaissent beaucoup sur nous : ce qu'on lit, nos mails, ce qu'on achète, où on se trouve...<br><br>
Si demain on leur confie en plus notre santé, il y a des conflits d’intérêt majeurs que je vois se profiler. Que se passera t-il un jour quand une société comme Apple décidera de faire de l’assurance santé. Comme ils savent tout, ils font des capteurs, des montres et ont accès à différents paramètres de santé, si on est un peu parano et je suis payé pour l’être, ils auront l’idée de faire de l’assurance santé avec évidemment ce que rêve tout assureur, assurer des gens en bonne santé et de refuser des gens qui risquent de pas être en bonne santé et qui coutent cher. Et dans ce modèle la, tout modèle mutualiste explose. A moins d’être en bonne santé on ne sera plus assuré”

[https://www.economie.gouv.fr/direct-video-conference-presse-sur-application-stopcovid-23-juin#](https://www.economie.gouv.fr/direct-video-conference-presse-sur-application-stopcovid-23-juin#) <em><span style="color:orange;">23 juin</span></em>

- "La santé publique n'est pas un sujet commercial"
- "La portabilité reste une préoccupation". "Dans une phase de lancement il est normal d'aller au plus simple, maintenant il faut enclencher la portabilité"
- "En phase opérationnelle, une solution qualifiée par l'ANSSI et non soumise a des lois extraterritoriales non européenne sera de très bon goût"

# Conseil National de l'Ordre Des Medecins CNOM
[Medecins et patients dans le monde des data, des algorithmes et de l'intelligence artificielle](https://www.conseil-national.medecin.fr/sites/default/files/external-package/edition/od6gnt/cnomdata_algorithmes_ia_0.pdf)
- Recommandation #1 :  "Une personne et une société libres et non asservies par les géants technologiques. Ce principe éthique fondamental doit être réaffirmé à l´heure où les dystopies et les utopies, les plus excessives, sont largement médiatisées"
- Recommandation #33 : "Le Cnom alerte enfin sur le fait que les infrastructures de données, plateformes de collecte et d’exploitation, constituent un enjeu majeur sur les plans scientifique, économique, et en matière de cyber-sécurité. La localisation de ces infrastructures et plateformes, leur fonctionnement, leurs finalités, leur régulation représentent un enjeu majeur de souveraineté afin que, demain, la France et l’Europe ne soient pas vassalisées par des géants supranationaux du numérique"
- "Le respect des secrets des personnes est la base même de la confiance qu’elles portent aux médecins. Il faut donc mettre cette exigence éthique dans le traitement massif des data lors de la construction des algorithmes"

# Conseil d'Etat
[Conseil d'État, 19 juin 2020, Plateforme Health Data Hub](https://www.conseil-etat.fr/ressources/decisions-contentieuses/dernieres-decisions-importantes/conseil-d-etat-19-juin-2020-plateforme-health-data-hub) <em><span style="color:orange;">19 juin</span></em>

- "Dès lors, il y a lieu de prévoir que la Plateforme des données de santé complétera, dans un délai de cinq jours, les informations ainsi diffusées, pour mentionner, d'une part, le possible transfert de données hors de l'Union européenne, compte tenu du contrat passé avec son sous-traitant, et, d'autre part, les informations adaptées relatives aux droits des personnes concernées, compte tenu, le cas échéant, de l'impossibilité d'identifier ces dernières et de la nécessité du traitement pour l'exécution d'une mission d'intérêt public, envisagées aux articles 11 et 21 du règlement général sur la protection des données.""
- "La Plateforme des données de santé, d'une part, fournira à la Commission nationale de l'informatique et des libertés, dans un délai de cinq jours à compter de la notification de la présente décision, tous éléments relatifs aux procédés de pseudonymisation utilisés, propres à permettre à celle-ci de vérifier que les mesures prises assurent une protection suffisante des données de santé  traitées"

#  Conseil National des Barreaux CNB

[Motion portant sur la creation du GIP Plateforme des donnees de sante dit HealthDataHub](https://www.cnb.avocat.fr/sites/default/files/11.cnb-mo2020-01-11_ldh_health_data_hubfinal-p.pdf)
- "MET EN GARDE contre les risques de violation du secret médical et d’atteinte au droit à la vie privée"

# Comite consultatif National d'Ethique CCNE

[Données massives et santé : une nouvelle approche des enjeux éthiques](https://www.ccne-ethique.fr/sites/default/files/publications/avis_130.pdf)
- "Face au défi technologique que posent, pour la souveraineté nationale et européenne, le stockage, le partage et le traitement des données massives dans le domaine de la santé, le CCNE préconise le développement de plateformes nationales mutualisées et interconnectées. Ouvertes selon des modalités qu’il faudra définir aux acteurs publics et privés, elles doivent permettre à notre pays et à l’Europe de préserver leur autono-mie stratégique et de ne pas perdre la maîtrise de la richesse que constituent les données, tout  en privilégiant un partage contrôlé qui est indispensable à l’efficacité du soin et de la recherche médicale."

[Numérique et Santé : Quels enjeux éthiques pour quelles régulations ?](https://www.ccne-ethique.fr/sites/default/files/publications/rapport_numerique_et_sante_19112018.pdf)

# Article de loi / officiel

[Décret n° 2020-551 du 12 mai 2020 relatif aux systèmes d'information](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000041869923&dateTexte=&categorieLien=id) <em><span style="color:orange;">12 mai</span></em>

[Arrêté : épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000041812986&dateTexte=20200421) <em><span style="color:orange;">21 avril</span></em>

[Article 48 - RGPD - "Transferts ou divulgations non autorisés par le droit de l'Union"](https://www.privacy-regulation.eu/fr/48.htm)
- "Toute décision d'une juridiction ou d'une autorité administrative d'un pays tiers exigeant d'un responsable du traitement ou d'un sous-traitant qu'il transfère ou divulgue des données à caractère personnel ne peut être reconnue ou rendue exécutoire de quelque manière que ce soit qu'à la condition qu'elle soit fondée sur un accord international, tel qu'un traité d'entraide judiciaire, en vigueur entre le pays tiers demandeur et l'Union ou un État membre, sans préjudice d'autres motifs de transfert en vertu du présent chapitre."

[Health Data Hub mission de préfiguration](https://solidarites-sante.gouv.fr/IMG/pdf/181012_-_rapport_health_data_hub.pdf)
- "Le patrimoine de données de santé est une richesse nationale et chacun en prend peu à peu conscience.[...] La souveraineté et l´indépendance de notre système de santé face aux intérêts  étrangers, ainsi que la compétitivité de notre recherche et de notre industrie dépendront de la vitesse de la France à s´emparer du sujet."

[Confirmation de Microsoft au Sénat](http://www.senat.fr/basile/visio.do?id=qSEQ200114130&idtable=q371194%7Cq370883%7Cq370105%7Cq369641%7Cq368627%7Cq371617%7Cq371538%7Cq371754%7Cq371099%7Cq371232&_c=recherche&rch=qs&de=20170205&au=20200205&dp=3+ans&radio=dp&aff=sep&tri=dd&off=0&afd=ppr&afd=ppl&afd=pjl&afd=cvn)

[Naissance HealthDataHub : LOI n° 2019-774 du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038821260&categorieLien=id)

[Loi pour une République numérique](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033202746&categorieLien=id)
- "veiller à préserver la maîtrise, la pérennité et l’indépendance de son système d’information"
- "encourager l’utilisation des logiciels libres et des formats ouverts lors du développement, de l’achat ou de l’utilisation, de tout ou partie, de ce système d’information"

# CloudAct - RGPD
[Mme Denis : CloudAct vs. RGPD](http://videos.assemblee-nationale.fr/video.8070927_5d6f64b41f5fe.commission-speciale-bioethique--auditions-diverses-4-septembre-2019?timecode=15077543)
- "S´agissant du Cloud-Act [...] ce qui est certain est qu´il y a un conflit de loi sur l´article 48 du RGPD"

[Évaluation du CCBE de la loi CLOUD Act des États-Unis](https://www.ccbe.eu/fileadmin/speciality_distribution/public/documents/SURVEILLANCE/SVL_Position_papers/FR_SVL_20190228_CCBE-Assessment-of-the-U-S-CLOUD-Act.pdf)
- "En  tant  que  destinataires  d'un  mandat  en  vertu  du CLOUD  Act,  les  entreprises  technologiques  se  trouveront entre deux lois contradictoires sur les données. Étant donné que le RGPD limite strictement les circonstances dans lesquelles les données peuvent être légalement transférées à des pays tiers et prévoit  de  lourdes  amendes  en  cas  de  violation  (allant jusqu'à  4  %  du  chiffre  d'affaires  d'une  entreprise), les entreprises sont prises entre le non-respect d'un mandat des États-Unis (pour violation d'une ordonnance du CLOUD Act) et le risque de sanctions monétaires voire pénales importantes (pour violation des dispositions du RGPD)"
- "Le CLOUD Act menace de compromettre l'inviolabilité du conseil juridique sans offrir aux avocats ou à leurs clients aucune garantie procédurale pour protéger le secret professionnel accordé par le droit de l’UE."

[Rapport Gauvain](https://www.vie-publique.fr/sites/default/files/rapport/pdf/194000532.pdf) (p28-30)
- "Les données à caractère personnel des personnes physiques, comme les données des personnes morales sont concernées"
- "Au total, quoi qu’il en soit, le champ des infractions concernées est très vaste et comprend sans aucun  doute  l’ensemble  des  infractions  de  nature  économique,  commerciale  et  financière susceptibles d’affecter les entreprises françaises"

[Rapport Longuet](http://www.senat.fr/notice-rapport/2019/r19-007-1-notice.html)
- "pour répondre à cette exigence [de maîtrise des données], mais aussi afin de réaliser, autant que possible, des économies d'acquisition, de gestion, de maintenance et de formation, plusieurs administrations ont fait le choix de développer leurs propres solutions informatiques, à partir de logiciels dont les codes sources sont publics. C'est, par exemple, le cas de la Gendarmerie qui, depuis 2009, a équipé les 80.000 postes informatiques de ses services de solutions informatiques libres qui lui ont permis de regagner son indépendance et sa souveraineté vis-à-vis des éditeurs privés. Il serait très utile de réaliser rapidement le bilan de cette expérience unique et d'évaluer les possibilités de son extension à d'autres ministères."

[Arnaud Coustillière, directeur général de la DGNUM](https://www.dailymotion.com/video/x7mhqgf)
- "On a besoin de partenaires de confiance" 
  - "un cadre juridique sans extraterritorialité"
  - "une communauté de destin"

[Microsoft devient le premier hébergeur de données de santé certifié en France](https://www.usinenouvelle.com/article/microsoft-devient-le-premier-hebergeur-de-donnees-de-sante-certifie-en-france.N765904)
- "Microsoft s’investit beaucoup dans le big data et le machine learning en santé en France"

[Selon l’avocat général Saugmandsgaard Øe, la décision 2010/87/UE de la Commission relative aux clauses contractuelles types pour le transfert de données à caractère personnel vers des sous-traitants établis dans des pays tiers est valide](https://curia.europa.eu/jcms/upload/docs/application/pdf/2019-12/cp190165fr.pdf)

# Importance de la santé pour les Gafam
[How the “Big 4” Tech Companies Are Leading Healthcare Innovation](How the “Big 4” Tech Companies Are Leading Healthcare Innovation)
- "Selon Business Insider, Google Venture investit environ un tiers de ses fonds dans des startups de la santé et des sciences de la vie."

[Owkin annonce une extension de sa série A et atteint 18 millions de dollars levés](https://www.maddyness.com/2018/05/23/medtech-owkin-leve-11-millions-mettre-lia-service-de-recherche-medicale/)
- "Owkin annonce l’extension de sa série A auprès de Google Venture"

# Articles scientifiques
[Estimating the success of re-identifications in incomplete datasets using generative models](https://www.nature.com/articles/s41467-019-10933-3.pdf)
- "nous constatons que 99,98% des Américains seraient correctement ré-identifiés dans n'importe quel ensemble de données en utilisant 15 attributs démographiques"

[The Biggest Cybersecurity Threats Are Inside Your Company](https://hbr.org/2016/09/the-biggest-cybersecurity-threats-are-inside-your-company)
- "IBM a constaté que 60% de toutes les attaques étaient perpétrées par des initiés"

# Alternatives
[Teralab](https://www.teralab-datascience.fr/)
- "DATA SCIENCE FOR EUROPE : Plateforme d'intelligence artificielle et de big data"

[Elixir](https://elixir-europe.org/services/covid-19)
- "ELIXIR est une organisation intergouvernementale qui rassemble des ressources en sciences de la vie de toute l'Europe. Ces ressources comprennent des bases de données, des outils logiciels, du matériel de formation, du stockage en nuage et des superordinateurs. L'objectif d'ELIXIR est de coordonner ces ressources afin qu'elles forment une infrastructure unique. "

[GAIA-X](https://www.dotmagazine.online/issues/on-the-edge-building-the-foundations-for-the-future/gaia-x-a-vibrant-european-ecosystem)
- "GAIA-X: développer un écosystème européen dynamique"
- "GAIA-X est un projet ambitieux et opportun qui créera une infrastructure de données européenne efficace, sécurisée, distribuée et souveraine dans le dialogue entre l'État, l'industrie et la recherche"
- "Cependant, GAIA-X ne vise pas à construire le prochain fournisseur mondial de cloud, mais plutôt à permettre la participation des nombreux fournisseurs spécialisés à travers l'Europe et à les inclure dans une infrastructure de données distribuée et interconnectée"

[Le CERN revient sur son projet Microsoft Alternatives (MALt) pour passer sur des solutions open source](https://www.nextinpact.com/brief/le-cern-revient-sur-son-projet-microsoft-alternatives--malt--pour-passer-sur-des-solutions-open-source-9025.htm)
- "les conditions financières attractives qui avaient initialement attiré les responsables de services du CERN ont peu à peu disparu pour être remplacées par des systèmes basés sur des licences et des modèles commerciaux adaptés au secteur privé"

[OVH-Outscale : le cloud souverain vraiment ressuscité ?](https://www.lemondeinformatique.fr/actualites/lire-ovh-outscale-le-cloud-souverain-vraiment-ressuscite-76657.html)

[Health Data Hub : un collectif critique le choix Microsoft](https://www.nextinpact.com/news/108781-health-data-hub-collectif-critique-choix-microsoft.htm)
- "Le choix d’avoir recours à l’entreprise américaine Microsoft pour l’hébergement des données, sans qu’il n’y ait eu d’appel d’offre ou de mise en concurrence préalable interpellent les acteurs du logiciel libre français"
