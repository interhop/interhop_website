---
layout: post
title: "Mr Snowden, Thank You"
permalink: /snowden-merci
ref: snowden-merci
lang: fr
---

Merci aux lanceurs d'alerte.<br>
Merci d'[alerter à nouveau l'Europe.](https://www.lepoint.fr/monde/trente-cinq-chefs-d-etat-etaient-sous-ecoute-de-la-nsa-24-10-2013-1747689_24.php)

[![](https://i.ibb.co/JzSb3Y5/Screenshot-2020-05-20-at-11-04-21.png)](https://twitter.com/Snowden/status/1262812073353981954?s=20)

<!-- more -->

[Le gouvernement contraint les hôpitaux à abandonner vos données chez Microsoft](https://interhop.org/le-gouvernement-contraint-les-hopitaux-a-abandonner-vos-donnees-chez-microsoft)

[French government forces hospitals to give up health data to Microsoft](https://interhop.org/le-gouvernement-contraint-les-hopitaux-a-abandonner-vos-donnees-chez-microsoft_en/)
