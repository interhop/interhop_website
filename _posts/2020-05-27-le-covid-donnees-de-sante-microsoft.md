---
layout: post
title: "Le Covid, les données de santé et Microsoft"
permalink: /covid-donnees-de-sante-microsoft
ref: covid-donnees-de-sante-microsoft
lang: fr
---

> « L’Europe juridique doit se réveiller, entraînée par la France et la pression de l’opinion. Elle doit proposer une troisième voie pour garantir un avenir numérique compatible avec nos démocraties » réclame dans cette tribune-pétition de nombreux professionnels de la santé et du numérique, entre autres. Un lien est en ligne pour la signer. 

<!-- more -->

[Signer ce papier](https://forms.interhop.org/node/20)

## Une information loyale et éclairée 

Le taux d'alphabétisation numérique - la capacité à lire et écrire le langage informatique - est extraordinairement bas[^alphabetisation]. Notre monde technophile s'envole dans la complexité écartant le citoyen du débat politique. Cependant les implications sont fondamentales : elles concernent la pérennité de notre liberté de penser et de notre système de santé mutualiste.<br>
Nous voulons informer des évolutions récentes en informatique et en santé pour recentrer politiquement le débat autour du citoyen et fermer la porte aux illusions techno-scientifiques.

## Vers le secret professionnel partagé

Récemment l'Ordre des Médecins a rappelé que le secret des personnes est la base de la confiance portée aux personnels soignants[^CNOM_secret]. <br>
Historiquement la relation de soin est construite sur un colloque singulier médecin-malade. <br>
Avec l'arrivée des nouvelles technologies et la numérisation du monde, le secret médical formel se dissipe peu à peu. Les personnels administratifs de l'Etat traitent déjà l'information médicale des assurés de l'Assurance Maladie[^Revel]. Les évolutions contemporaines tendent à multiplier les acteurs du parcours de soins dans une logique de prise en charge collective et pluridisciplinaire. Comment remettre en adéquation cette exigence éthique avec le XXIième siècle, à l'heure du traitement massif des données effectué par des algorithmes toujours plus sophistiqués ?

Les notions[^macsf] "d'équipe de soin" et de "secret partagé" redéfinissent les contours du secret à l'ère digitale. Les informations médicales strictement "nécessaires à la continuité des soins" peuvent circuler entre les différents membres de l’équipe de soins pour améliorer la prise en charge globale de la personne. De plus, l'opposition au partage de données[^opposition] est un droit essentiel pour le patient. Le préalable à tout refus est le droit à une information claire et précise sur les bénéfices attendus, les forces en présence et le parcours de la donnée médicale. La fracture numérique[^fracnum] et l'empressement à déployer des outils informatiques ne permettent plus d'assurer ces droits.

## Technophilie et centralisation

"Science sans conscience n’est que ruine de l’âme." Rabelais

C'est un principe éthique fondamental : "les technologies doivent être au service de la personne et de la société" plutôt qu'"asservies par les géants technologiques"[^CNOM_IA].

Une confiance aveugle dans la technologie, et notamment dans les nouveaux outils statistiques, pourrait conduire à  légitimer  des systèmes pyramidaux et potentiellement réducteurs de libertés.
Ainsi, les nouveaux systèmes d'information initiés dans le contexte de l'état d'urgence sont tous centralisés : recueil d'identités et de contacts à risque, résultats de prise de sang, surveillance des téléphones portables.<br>
Ces systèmes alimenteront la plateforme nationale des données de santé ou "Health Data Hub"[^theconversation_Fallery]. Ce guichet unique d’accès à l’ensemble des données de santé désidentifiées vise à développer l’intelligence artificielle appliquée à la santé. L'ensemble de ces données est hébergé par l'entreprise Microsoft et son offre commerciale, la plateforme Azure[^HDH_microsoft]. 

Le problème est que le droit américain s'applique au monde entier !<br> 
Le gouvernement américain a adopté en 2018 un texte nommé "Cloud Act"[^cloudact], qui permet à la justice américaine d’avoir accès aux données stockées dans les pays tiers[^CCBE_cloud_act]. Microsoft est soumis à ce texte qui est en conflit avec notre règlement européen sur la protection des données[^CNIL_denis].

Comment soutenir ce choix alors que le Président de l'Agence Nationale de Sécurité des Systèmes d'Information s'oppose publiquement aux géants du numérique qui représenteraient une attaque pour nos systèmes de "santé mutualiste"[^ANSSI] ?<br>
Comment soutenir ce choix alors que la CNIL mentionne dans le contrat liant le "Health Data Hub" à Microsoft "l’existence de transferts de données en dehors de l’Union européenne dans le cadre du fonctionnement courant de la plateforme"[^CNIL_microsoft] ?<br>
Comment soutenir ce choix alors qu'existent des dizaines d'alternatives industrielles françaises et européennes[^HDS] ?

## Troisième voie : autonomie / Europe

Brutalement, l’affaire Snowden a montré l'utilisation de nos données au travers de programmes de surveillance mondialisés[^snowden_europ].<br>
Brutalement, le confinement nous a fait vivre dans nos chairs des privations de liberté imposées et nécessaires.

Au cœur de l'économie du XXIième siècle, les données ont progressivement pris une importance cruciale. Elles sont le  pétrole de nos économies modernes[^theeconomist] et celui qui les contrôle, s’impose. Ces dernières sont exploitées par des États-plateformes dépendants des forces du marché (Google, Apple, Facebook, Amazon, Microsoft) ou d'un régime autoritaire (Baidu, Alibaba, Tencent et Xiaomi)[^internet_chinois].

Nous devons revenir aux principes politiques de base[^prefiguration] et comprendre que "le patrimoine de données de santé est une richesse nationale.[...] La souveraineté et l´indépendance de notre système de santé face aux intérêts  étrangers, ainsi que la compétitivité de notre recherche et de notre industrie, dépendront de la vitesse de la France à s´emparer du sujet."

L'autonomie des personnes doit être renforcée. La délivrance d'une information éclairée et transparente regroupant patients, professionnels de santé et législateurs doit être réalisée. Ensuite les personnes pourront s'opposer à l'envoi de données les concernant en dehors du cadre juridique qui les défend. 

L'Europe juridique doit se réveiller, entraînée par la France et la pression de l’opinion. Elle doit proposer une troisième voie pour garantir un avenir numérique compatible avec nos démocraties. "L’enjeu fondamental pour les Européens est d’être en mesure de conserver leur autonomie de pensée."[^libertepenser]
Il revient donc aux législateurs européens et français de protéger la démocratie à l’ère du capitalisme de surveillance. La Cour de Justice de l'Union Européenne[^cjue] ainsi que les régulateurs nationaux des données personnelles doivent se positionner quant à la possibilité de contractualisation avec des entreprises assujetties aux lois américaines.

L'initiative franco-allemande GAIA-X[^gaiax] qui veut fournir un cadre technique de transparence et de bonne conduite aux états-plateformes mondialisés, doit être propulsée par l'Union Européenne. C'est une absolue nécessité.
Pour une Europe Numérique autonome, il est nécessaire d'utiliser "des logiciels libres et des formats ouverts lors du développement"[^rep_num] des systèmes d'informations. Pour que demain, chacun ait accès à des soins de qualité, exigeons la publication des plans d'architecture des plateformes informatiques, des flux de données, des algorithmes et des terminologies médicales. 

En outre, la qualité de la recherche se co-construit avec des équipes pluridisciplinaires et des interactions entre soin et recherche. Sur place, au sein des hôpitaux et des structures participant au service public, il faut encourager les "communs" et l'autonomie d'un tissu d'enseignants, de chercheurs, d'informaticiens, de soignants ainsi qu'un réseau de personnes en confiance avec l'ensemble du système. Dans ce maillage, la place des régions et des hôpitaux doit être renforcée.

Si le cadre juridique et théorique doit venir de l'Europe, la mise en conformité avec le réel restera locale.


---

[Signer ce papier](https://forms.interhop.org/node/20)

[Voir l'article dans Médiapart](https://blogs.mediapart.fr/les-invites-de-mediapart/blog/270520/le-covid-les-donnees-de-sante-et-microsoft)

---

#### Résumé 
- Secret médical formel -> secret partagé à l'équipe de soin et renforcement du secret profesionnel
  - Colloque singulier -> Acteurs multiples
- Dans tous les cas SIDEP, Contact trancing, HDH... : centralisation
  - Le tout chez Microsoft
  - Présentation de l'avis ANSSI et CNIL
- Solutions :
  - Autonomie des personnes : information et opposition
  - Autonomie juridique : CJUE -> avis de la CNIL
  - Autonomie technique : GAIAX, Europe numérique autonome
  - Autonomie locale et commun : hopitaux, région

#### Sources

[^HDS]: [Annuaire des hébergeurs aggréés AFHADS](https://www.afhads.fr/wp-content/uploads/2018/05/6-Annuaire-des-membres.pdf)

[^theconversation_Fallery]: [Données de santé : l’arbre StopCovid qui cache la forêt Health Data Hub](https://theconversation.com/donnees-de-sante-larbre-stopcovid-qui-cache-la-foret-health-data-hub-138852)

[^alphabetisation]: [L'alphabétisation numérique et la participation familiale à l'école](https://www.cairn.info/revue-la-revue-internationale-de-l-education-familiale-2014-1-page-55.htm)

[^CNOM_secret]: [Plan de déconfinement et garantie du secret médical](https://www.conseil-national.medecin.fr/publications/communiques-presse/plan-deconfinement-garantie-secret-medical)

[^theeconomist]: [The world’s most valuable resource is no longer oil, but data](https://www.economist.com/leaders/2017/05/06/the-worlds-most-valuable-resource-is-no-longer-oil-but-data)

[^macsf]: [Comment concilier respect du secret professionnel et efficacité des soins ?](https://www.macsf.fr/Responsabilite-professionnelle/Relation-au-patient-et-deontologie/concilier-secret-professionnel-efficacite-des-soins)

[^prefiguration]: [HealthDataHub : Mission de préfiguration](https://solidarites-sante.gouv.fr/IMG/pdf/181012_-_rapport_health_data_hub.pdf)

[^Revel]: ["Le secret médical sera préservé"](https://www.franceinter.fr/emissions/l-invite-du-week-end/l-invite-du-week-end-10-mai-2020)

[^opposition]: [Opposition et information](https://www.cnil.fr/fr/respecter-les-droits-des-personnes)

[^CNOM_IA]: [Médecins et Patients dans le monde des data, des algorithmes et de l'intelligence artificielle](https://www.conseil-national.medecin.fr/sites/default/files/external-package/edition/od6gnt/cnomdata_algorithmes_ia_0.pdf)

[^internet_chinois]: ["Enquête. Quand Internet sera chinois"](https://pad.interhop.org/MmdhzZ6hS0KfGJUkfzSoqw#)

[^HDH_microsoft]: [Modalités de stockage du « health data hub »](http://www.senat.fr/basile/visio.do?id=qSEQ200114130&idtable=q371194%7Cq370883%7Cq370105%7Cq369641%7Cq368627%7Cq371617%7Cq371538%7Cq371754%7Cq371099%7Cq371232&_c=recherche&rch=qs&de=20170205&au=20200205&dp=3+ans&radio=dp&aff=sep&tri=dd&off=0&afd=ppr&afd=ppl&afd=pjl&afd=cvn)

[^snowden_europ]: [Trente-cinq chefs d'État étaient sous écoute de la NSA](https://www.lepoint.fr/monde/trente-cinq-chefs-d-etat-etaient-sous-ecoute-de-la-nsa-24-10-2013-1747689_24.php)

[^gaiax]: [Franco-German Position on GAIA-X](https://www.bmwi.de/Redaktion/DE/Downloads/F/franco-german-position-on-gaia-x.pdf?__blob=publicationFile&v=10)

[^libertepenser]: ["Thomas Gomart (IFRI) : « Le Covid-19 accélère le changement de mains de pans entiers de l’activité économique au profit des plateformes numériques »"](https://pad.interhop.org/94WL-H95QCaJv5vBKJzm4g)

[^cjue]: [Données personnelles : le transfert vers les Etats-Unis validé par la CJUE](https://www.lesechos.fr/tech-medias/hightech/donnees-personnelles-le-transfert-vers-les-etats-unis-valide-par-la-cjue-1157955)

[^cloudact]: [Rapport Gauvain : Rétablir la souveraineté de la France et de l’Europe et protéger nos entreprises des lois et mesures à portée extraterritoriale](https://www.vie-publique.fr/sites/default/files/rapport/pdf/194000532.pdf)

[^CNIL_denis]: [Commission spéciale Bioéthique : Auditions diverses, Mme DENIS](http://videos.assemblee-nationale.fr/video.8070927_5d6f64b41f5fe.commission-speciale-bioethique--auditions-diverses-4-septembre-2019?timecode=15077543)

[^CCBE_cloud_act]: [Évaluation du CCBE de la loi CLOUD Act des États-Unis](https://www.ccbe.eu/fileadmin/speciality_distribution/public/documents/SURVEILLANCE/SVL_Position_papers/FR_SVL_20190228_CCBE-Assessment-of-the-U-S-CLOUD-Act.pdf)

[^ANSSI]: [Audition de M. Guillaume Poupard, directeur général de l'Agence nationale de la sécurité des systèmes d'information (ANSSI)](https://videos.senat.fr/video.1608918_5ebaa04d6b8cd.audition-de-m-guillaume-poupard-directeur-general-de-l-agence-nationale-de-la-securite-des-systeme?timecode=4319000)

[^CNIL_microsoft]: [Délibération n° 2020-044 du 20 avril 2020 portant avis sur un projet d'arrêté complétant l’arrêté du 23 mars 2020 prescrivant les mesures d’organisation et de fonctionnement du système de santé nécessaires pour faire face à l’épidémie de covid-19 dans le cadre de l’état d’urgence sanitaire](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf)

[^rep_num]: [LOI n° 2016-1321 du 7 octobre 2016 pour une République numérique](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033202746&categorieLien=id)

[^fracnum]: [Les quatre dimensions de la fracture numérique](https://www.cairn.info/revue-reseaux1-2004-5-page-181.htm)
