---
layout: post
title: "Pour une Intelligence Artificielle (IA) éthique!"
ref: sondage_ia
lang: fr
---

**interhop.org** partage librement des algorithmes pour promouvoir une IA utile pour les personnels de santé et le patient.

Voulez-vous nous aider à prioriser nos travaux ?

<!-- more -->

Merci de répondre à ce questionnaire pour cibler les besoins (< 1 min) : [forms.interhop.org](https://forms.interhop.org/node/1)
