---
layout: post
title: "Mr Stallman, Merci"
permalink: /stallman-merci
ref: stallman-merci
lang: fr
---

Richard Stallman, merci d'avoir signé notre [tribune](https://interhop.org/covid-donnees-de-sante-microsoft) dans Médiapart.

Sur le sujet de la souveraineté et autonomie numérique, Richard Stallman a publié un [article](https://www.gnu.org/government/) : "Mesures à la portée des gouvernements pour promouvoir le logiciel libre et pourquoi c'est leur devoir de le faire"

<!-- more -->

> "Chaque génération a son philosophe, un écrivain ou un artiste qui capte l'air du temps. Quelquefois, ces philosophes sont reconnus comme tels ; souvent cela prend des générations avant qu'ils ne soient reconnus. Mais reconnus ou pas, une époque reste marquée par les gens qui parlent de leurs idéaux, dans le murmure d'un poème ou l'explosion d'un mouvement politique.<br><br>
> Notre génération a un philosophe. Il n'est ni artiste, ni écrivain professionnel. Il est programmeur. Richard Stallman a débuté son travail dans les laboratoires du MIT, comme programmeur et architecte de systèmes d'exploitation. Il a bâti sa carrière sur la scène publique comme programmeur et architecte fondateur d'un mouvement pour la liberté, dans un monde de plus en plus défini par le « code ». 

> [Lawrence Lessig](https://www.gnu.org/philosophy/lessig-fsfs-intro.fr.html), professeur de droit, Stanford Law School

