---
layout: post
title: "Communiqué de Presse : courrier à la CNIL"
permalink: /communique-presse-cnil-cjue-schrems
ref: communique-presse-cnil-cjue-schrems
lang: fr
---

Envoi à la Commission Nationale de l'Informatique et des Libertés CNIL d'un courrier faisant suite à l'**annulation  de l’accord sur le transfert de données personnelles entre l’Europe et les États-Unis ou "Privacy Shield"**.

<!-- more -->

# Objet

Demande d’explication concernant les implications sur la “Plateforme des Données de Santé” de l’annulation du “bouclier de protection des données” ou “Privacy Shield”.

# Contenu de la lettre

Madame La Présidente,

Le déploiement d’une “Plateforme des Données de Santé” par la création d’un Groupement d’Intérêt Public, a été proclamé par la loi n° 2019-774 du 24 juillet 2019 relative à l’organisation et à la transformation du système de santé[^loisante].
Actuellement ces données sont stockées chez Microsoft Azure[^senat_microsoft], cloud public du géant américain Microsoft. 

L'avis n° 2020-044 du 20 avril 2020 de votre Commision[^CNILdel] évoque  les risques de transferts de données vers des pays tiers et les divulgations non autorisées par le droit de l’Union [Européenne] dans le cadre du contrat de sous-traitance de la solution technique de la "plateforme" à Microsoft Azure.
Suivant cet avis, le 19 juin 2020, le Conseil d'État a enjoint la "Plateforme des Données de Santé" d'informer les citoyens du "possible transfert de données hors de l'Union Européenne, compte tenu du contrat passé avec son sous-traitant"[^conseil_etat].
Nous attirons l'attention sur **la distinction entre les données "de repos" et les données "de transit"**. Lors de l'audience au Conseil d'État le 11 juin, il a été évoqué que même si **Microsoft pouvait garantir la localisation des données de repos (i.e. le stockage), ce n'était pas le cas pour les données de transit (i.e. d'analyse), qui une fois copiées sur les processeurs circulent dans le monde entier.** Nous ne parlons ici pas des données de maintenance qui ne sont pas des données de santé.

Le 16 juillet, la Cour de Justice de l'Union Européenne déclare donc que "les limitations de la protection des données à caractère personnel qui découlent de la réglementation interne des États-Unis portant sur l’accès et l’utilisation, par les autorités publiques américaines, de telles données transférées depuis l’Union [Européenne] vers ce pays tiers ne sont pas encadrées d’une manière à répondre à des exigences équivalentes à celles requises, en droit de l’Union [Européenne], par le principe de proportionnalité, en ce que les programmes de surveillance fondés sur cette réglementation ne sont pas limités au strict nécessaire"[^curia].

Suite à cet arrêt, le Régulateur Allemand souligne que "les données ne devraient pas être transférées aux États-Unis tant que ce cadre juridique n'aura pas été réformé"[^CNIL_allemande].

Nous sollicitons votre expertise pour connaître les conséquences de l'ensemble des données de santé de plus de 67 millions de personnes chez Microsoft Azure au sein de la "Plateforme des Données de Santé".
Plus particulièrement, **nous souhaitons connaître les conséquences de la suppression du "Privacy Shield" sur le fonctionnement de la "Plateforme des Données de Santé"**, surtout qu'au moment de la saisine du Conseil d'État en reféré liberté, et jusqu'à son Ordonnance, le texte sus-cité était toujours en vigueur.

Nous avons décidé de rendre publique ce courrier. Cette publicité contribue à l’objectif de transparence défendu par votre Commission[^stopcovid_cnil].

Dans cette attente, nous vous prions de croire, Madame la Présidente, en l’expression de notre très haute considération.

Association interhop.org<br>

[^stopcovid_cnil]: [Application « StopCovid » : la CNIL tire les conséquences de ses contrôles](https://www.cnil.fr/fr/application-stopcovid-la-cnil-tire-les-consequences-de-ses-controles)

[^CNIL_allemande]: [Berlin: Berlin Commissioner issues statement on Schrems II case, asks controllers to stop data transfers to the US](https://www.dataguidance.com/news/berlin-berlin-commissioner-issues-statement-schrems-ii-case-asks-controllers-stop-data)

[^curia]: [La Cour invalide la décision 2016/1250 relative à l'adéquation de la protection assurée par le bouclier de protection des données UE-États-Unis](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

[^arrete_sortie_etat_urgence]: [ Arrêté du 10 juillet 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans les territoires sortis de l'état d'urgence sanitaire et dans ceux où il a été prorogé](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000042106233&dateTexte=&categorieLien=id)

[^ccomptes]: [LES DONNÉES PERSONNELLES DE SANTÉ GÉRÉES PAR L'ASSURANCE MALADIE](https://www.ccomptes.fr/sites/default/files/EzPublish/20160503-donnees-personnelles-sante-gerees-assurance-maladie.pdf)

[^senat_appeloffre]: [Protection des données de santé : MORIN-DESAILLY Catherine](http://videos.senat.fr/video.1710660_5f10400c7efbf.seance-publique-du-16-juillet-2020-apres-midi?timecode=16471000


[^reunin_stopcovid]: [Conférence de presse sur l'application StopCovid, le 23 juin](https://www.economie.gouv.fr/direct-video-conference-presse-sur-application-stopcovid-23-juin#)


[^senat_microsoft]: [Modalités de stockage du « health data hub »](http://www.senat.fr/basile/visio.do?id=qSEQ200114130&idtable=q371194%7Cq370883%7Cq370105%7Cq369641%7Cq368627%7Cq371617%7Cq371538%7Cq371754%7Cq371099%7Cq371232&_c=recherche&rch=qs&de=20170205&au=20200205&dp=3+ans&radio=dp&aff=sep&tri=dd&off=0&afd=ppr&afd=ppl&afd=pjl&afd=cvn)

[^CNILdel]: [Mesures d’organisation et de fonctionnement du système de santé nécessaires pour faire face à l’épidémie de covid-19 dans le cadre de l’état d’urgence sanitaire ](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf)

[^loisante]: [LOI n° 2019-774 du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038821260&categorieLien=id)

[^theconversation]: [The Conversation - Données de santé : l’arbre StopCovid qui cache la forêt Health Data Hub ](https://theconversation.com/donnees-de-sante-larbre-stopcovid-qui-cache-la-foret-health-data-hub-138852)

[^lenouvelobs]: [Le Nouvel Obs - Nos données de santé à Microsoft ? « On offre aux Américains une richesse nationale unique au monde »](https://www.nouvelobs.com/sante/20200623.OBS30391/nos-donnees-de-sante-a-microsoft-on-offre-aux-americains-une-richesse-nationale-unique-au-monde.html)

[^lemonde]: [Le Monde - « La politique publique des données de santé est à réinventer »](https://www.lemonde.fr/idees/article/2020/06/04/la-politique-publique-des-donnees-de-sante-est-a-reinventer_6041706_3232.html)

[^lepoint]: [Le Point - Health Data Hub : « Le choix de Microsoft, un contresens industriel ! »](https://amp.lepoint.fr/2379394?utm_term=Autofeed&utm_medium=Social&utm_source=Twitter&Echobox=1591814194&__twitter_impression=true)

[^conseil_etat]: [Conseil d'État, 19 juin 2020, Plateforme Health Data Hub](https://www.conseil-etat.fr/ressources/decisions-contentieuses/dernieres-decisions-importantes/conseil-d-etat-19-juin-2020-plateforme-health-data-hub)
