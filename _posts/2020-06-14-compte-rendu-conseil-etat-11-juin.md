---
layout: post
title: "Audition au Conseil d'Etat du 11 juin"
permalink: /compte-rendu-conseil-etat-11-juin
ref: compte-rendu-conseil-etat-11-juin
lang: fr
---

Un collectif d'entreprises et d'associations défenseuses du logiciel libre[^mediapart] ainsi que de patients et de médecins - dont Pr Didier Sicard - s'élève contre l’arrêté du Ministre des Solidarités et de la Santé du 21 avril 2020[^arrete_vingt_un] prescrivant les mesures d'organisation et de fonctionnement du système de santé nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire.

Il est demandé au Ministre des Solidarités et de la Santé de suspendre cet arrêté en ce qu’il confie la collecte et le traitement de données de santé à la plateforme Health Data Hub HDH, hébergée chez l'offre Cloud de Microsoft.

Il doit être mis fin aux atteintes graves et manifestement illégales au droit au respect de la vie privée et au droit à la protection des données.

Le caractère sensible de la données de santé, la non minimisation de la collecte et le transfert irréversible des données en dehors de notre juridiction européenne sont pointés.

[^mediapart]: [Le Health Data Hub attaqué devant le Conseil d’Etat](https://www.mediapart.fr/journal/france/090620/le-health-data-hub-attaque-devant-le-conseil-d-etat?onglet=full)

[^arrete_vingt_un]: [Arrêté du 21 avril 2020 complétant l'arrêté du 23 mars 2020 prescrivant les mesures d'organisation et de fonctionnement du système de santé nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000041812986&dateTexte=20200421)

<!-- more -->

## Compte-rendu

### Concernant le choix de Microsoft

Microsoft, prestataire technique du Health Data Hub, est certifié hébergeur de données de santé, et notamment pour l'activité 5[^HDS] soit l'infogérance qui prévoit "l'administration et l'exploitation du système d'information contenant les données de santé"[^apssis]. 

Le HDH reconnait ne pas être certifié pour l'infogérence. 
Cependant la manipulation, transfert, et gestion des données sera réalisée par les équipes du HDH.

Le HDH, étant soumis au référentiel du SNDS[^ref_secu_snds], se considère comme étant d'un niveau de sécurité plus important. Cette  homologation aurait été réalisée en présence de la CNIL et de l'ANSSI. Il s'agit d'une auto-homologation prononcée par la direction du HDH.

Comme indiqué dans le rapport de la CNIL sur l'arrêté du 21 avril 2020[^arrete_vingt_un], Microsoft garantit la localisation des données au repos, à savoir les sauvegardes. On apprend que les données au repos sont localisées aux Pays-Bas. 
Par opposition, Microsoft ne garantit pas la localisation des données dites de transit, c'est à dire les données en cours d'analyse. 

[^HDS]: [Liste des hébergeurs certifiés](https://esante.gouv.fr/labels-certifications/hds/liste-des-herbergeurs-certifies)

[^apssis]: [Point d’actualité sur la certification hébergeur de données de santé](https://www.apssis.com/actualite-ssi/335/point-d-actualite-sur-la-certification-hebergeur-de-donnees-de-sante.htm)

[^ref_secu_snds]: [Référentiel de sécurité SNDS ](https://www.snds.gouv.fr/download/Guide_accompagnement.pdf)

### Concernant la réversibilité et la portabilité de la plateforme


Les échanges récents avec OVH, par tweet interposés sont rappelés[^tweet_un] [^tweet_deux] [^tweet_trois]. Une étude concluerait à un différentiel net entre Microsoft et OVH, en particulier sur la capacité à déployer des plateformes à la volée.

Les développements ont été réalisés avec l'outil Terraform qui permettrait d'utiliser simplement divers clouders[^Terraform].

La directrice du HDH évoque des discussions très récentes avec le clouder Scaleway qui auraient fait des éloges sur plateforme HDH.

La directrice du Health Data Hub rappelle qu'une  quarantaine de services Azure sont utilisés par le HDH, en accord avec  l'ANSSI.

[^Terraform]: [Terraform](https://fr.wikipedia.org/wiki/Terraform_(logiciel))

[^tweet_un]: [https://twitter.com/olesovhcom/status/1266419803247595520?s=19](https://twitter.com/olesovhcom/status/1266419803247595520?s=19)

[^tweet_deux]: [https://twitter.com/olesovhcom/status/1267032572695101440](https://twitter.com/olesovhcom/status/1267032572695101440)

[^tweet_trois]: [https://twitter.com/olesovhcom/status/1267510178108375040](https://twitter.com/olesovhcom/status/1267510178108375040)


### Bases de données actives sur le HDH

- les données du réseau "Oscour" (Organisation de la surveillance coordonnée des urgences) issues de Santé Publique France, permettant de suivre les passages aux urgences pendant l'épidémie[^zd]
- SIVIC : fichier établit suite aux attaques du Bataclan; réutilisé lors de la crise des gilets jaunes
- PMSI dit "Fast Track"[^mars_cinq_deux]

[^zd]: [Health Data Hub : des organisations dénoncent un passage en force du projet dans le contexte d'urgence sanitaire](https://www.zdnet.fr/actualites/health-data-hub-des-organisations-denoncent-un-passage-en-force-du-projet-dans-le-contexte-d-urgence-sanitaire-39905045.htm)

[^mars_cinq_deux]: [Elargissement du périmètre du Fast-Track](https://drop.interhop.org/r/WRy4V9WZvS#Xk3sLkILG68kEE83rNGrkhk85febw4hJ4zWZQDYZo6M=)

### Concernant la pseudonymisation

Les données issues de la Caisse Nationale d'Assurance Maladie sont pseudonymisées via la méthodologie "Fonction d'Occultation des Identifiants Nominatifs" FOIN1/FOIN2[^foin]. 
Un 3ième niveau de pseudonymisation est appliqué par le HDH[^avis_cnil_ving_un], qui permet d'avoir des pseudonymes différents dans chacun des projets au sein du HDH. 
Il a été rappelé que les données pseudonymisées sont des données personelles et qu'elles restent parmi les données les plus sensibles.

[^foin]: [SNDS : la CNIL tousse sur la sécurité du grand fichier de santé](https://www.silicon.fr/snds-cnil-tousse-securite-grand-fichier-sante-166527.html)

[^avis_cnil_ving_un]: [Mesures d’organisation et de fonctionnement du système de santé nécessaires pour faire face à l’épidémie de covid-19 dans le cadre de l’état d’urgence sanitaire](https://www.cnil.fr/sites/default/files/atoms/file/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf)

### Concernant le RGPD et le CloudAct

Maître JB. Soufron, avocat des requérants, est revenu sur le respect du RGPD, en particulier :
- le risque de transfert des données en vertu du Cloud Act
- l'information du transfert en vertu des articles 13 et 14 du RGPD

### Concernant l'état d'urgence et la pérénité des données

Le HDH n'aura plus d'existence légale à la fin de l'état d'urgence sanitaire. C'est d'ailleurs ce qui a justifié la publication de l'arrêté du 21 Avril[^arrete_vingt_un]. Un projet de décret d'application de la loi de juillet 2019[^juillet] visant à inscrire le fonctionnement de cette plateforme dans le droit commun et précisant la répartition des responsabilités est en attente.

Le HDH indique qu'à l'issue de l'état d'urgence les données ne seront pas effacées, mais archivées.

Le HDH rappelle qu'il est constitué comme un entrepôt de données. En particulier que l'approche consiste donc à amasser les données puis de poser des questions. Cela s'oppose à l'approche partimonieuse où l'on définit des problématiques de recherche, puis on rassemble les données nécessaires.

[^juillet]: [Naissance HealthDataHub : LOI n° 2019-774 du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038821260&categorieLien=id)
