---
layout: post
title: "For an ethical Artificial Intelligence (AI)!"
ref: sondage_ia
lang: en
---

**interhop.org** freely shares algorithms to promote useful AI for healthcare workers and the patient.

Do you want to help us prioritize our work?

<!-- more -->

Please answer this questionnaire to target needs (< 1 min): [forms.interhop.org](https://forms.interhop.org/node/1)
